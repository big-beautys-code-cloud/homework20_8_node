/**
 * 写一段同步程序可以指定创建的文件个数于内容
 * 优化点：
 *       很多同步在一个函数内,实现了所有功能(尽可能实现单一职责)
 *       目录都在当前目录

*/

let fs = require('fs');

for(let i = 1;i <= 9;i++){
    let fd = fs.openSync(`第${i}个文件`,`w`);//以读取模式打开文件，如果文件不存在则创建
    //异步写入
    fs.writeSync(fd,`哈哈哈${i}`,function(err){
        if(err){
            throw err;
        }
    });
fs.closeSync(fd);
}