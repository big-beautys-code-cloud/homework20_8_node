//简单文件写入
let fs = require('fs');

/**
 * 简单的同步写入
 * fs.writeFileSync(file, data[, options])
 * file 文件
 * data 数据
 * option 可不写 {flag:标识 r w a........}
 */
 //sync为同步写入
 fs.writeFileSync('read.txt',"现在学习简单的同步写入",{flag:'a'});


/**
 * 简单的异步的写入
 * fs.writeFile(file, data[, options],callback)
 * file 参考如上
 * data 参考如上
 * option参考如上
 * callback 回调函数
 * 
 */
fs.writeFile('resd2.txt',"简单的异步写入",function(err){
    if(err){
        console.log(err);
    }
    console.log(arguments);
})