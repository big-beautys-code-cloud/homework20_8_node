/**
 * 作业
 * 创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
 */

 //流方法
 let fs = require('fs');
 let os = require('os');
 let startMes = os.freemem();//开始的内存
 let readFileStream = fs.createReadStream("./big.txt");//流的读取
 let writeFileStream = fs.createWriteStream('./big2.txt');
 readFileStream.pipe(writeFileStream);//pipe从一个流到另一个流
 let endMes = os.freemem();//最后的内存
 console.log('内存剩余：'+(startMes-endMes)/1024/1024);

 
 //传统
 let start = os.freemem()
 fs.writeFileSync('tradition.txt',"传统的",{flag:'a'});
 let end = os.freemem();
 console.log('内存剩余2：'+(start-end)/1024/1024);



