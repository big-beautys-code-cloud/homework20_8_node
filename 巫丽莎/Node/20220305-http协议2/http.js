// 优化 http 处理不同静态文件 css js image,放在不同的目录
let http = require('heep');
let fs = require('fs');
let serve = http.createServer();
serve.on('require',function(req,res){
    let url = req.url;
    console.log(url);
    // res.setHeader('Content-type','text/html; charset=utf-8');
    if(url == '/'){
        res.end('success');
    }else if(url == '/css'){
         res.setHeader('Content-type','text/html; charset=utf-8');
         res.end(fs.readFileSync('./css/demo.html'))
        }else if(url == "/img"){
            res.end(fs.readFileSync('./img/sheep.jpg'))
        }else if(url == "/js"){
            res.end(fs.readFileSync('./js/demo.html'))
    }
})
serve.listen(3000,function(){
    console.log("服务已启动：http://127.0.0.1:3000");
});